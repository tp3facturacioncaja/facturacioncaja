﻿using System.Web;
using System.Web.Mvc;

namespace UPC.TP3.FacturacionCaja.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
