﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Negocio.CN;

namespace UPC.TP3.FacturacionCaja.Web.Areas.Mantenimientos.Controllers
{
    public class EmpleadosController : Controller
    {
        // GET: Mantenimientos/Empleados
        public ActionResult Index()
        {
            List<Empleado> empleados = new EmpleadoCN().GetEmpleados();
            ViewBag.empleados = empleados;
            return View();
        }

        // GET: Mantenimientos/Empleados/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Mantenimientos/Empleados/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mantenimientos/Empleados/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mantenimientos/Empleados/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Mantenimientos/Empleados/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mantenimientos/Empleados/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Mantenimientos/Empleados/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
