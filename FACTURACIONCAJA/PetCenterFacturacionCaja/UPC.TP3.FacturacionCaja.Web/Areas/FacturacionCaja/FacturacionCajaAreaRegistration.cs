﻿using System.Web.Mvc;

namespace UPC.TP3.FacturacionCaja.Web.Areas.FacturacionCaja
{
    public class FacturacionCajaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FacturacionCaja";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FacturacionCaja_default",
                "FacturacionCaja/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}