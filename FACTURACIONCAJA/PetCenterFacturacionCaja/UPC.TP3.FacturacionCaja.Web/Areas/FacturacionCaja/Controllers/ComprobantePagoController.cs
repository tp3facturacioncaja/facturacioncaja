﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Negocio.CN;

namespace UPC.TP3.FacturacionCaja.Web.Areas.FacturacionCaja.Controllers
{
    public class ComprobantePagoController : Controller
    {
        // GET: FacturacionCaja/ComprobantePago
        public ActionResult Index()
        {
            List<ComprobantePago> comprobantepagos = new ComprobantePagoCN().GetComprobantePagos();
            ViewBag.comprobantepagos = comprobantepagos;
            return View();
        }

        // GET: FacturacionCaja/ComprobantePago/Details/5
        public ActionResult Details(int id)
        {
            ComprobantePago comprobantepago = new ComprobantePagoCN().GetComprobantePagoById(id);
            ViewBag.comprobantepago = comprobantepago;
            return View();
        }

        // GET: FacturacionCaja/ComprobantePago/Create
        public ActionResult Create()
        {
            List<Empleado> empleados = new EmpleadoCN().GetEmpleados();
            ViewBag.empleados = empleados;
            List<TurnoCaja> turnoCajas = new TurnoCajaCN().GetTurnoCajas();
            ViewBag.turnoCajas = turnoCajas;
            List<TipoMoneda> tipoMonedas = new TipoMonedaCN().GetTipoMonedas();
            ViewBag.tipoMonedas = tipoMonedas;

            return View();
        }

        // POST: FacturacionCaja/ComprobantePago/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            ComprobantePago comprobantePago = new ComprobantePago();

            
            comprobantePago.montoRecibido = Double.Parse(collection["comprobantePago.montoRecibido"]);
            comprobantePago.tipoComprobante = collection["comprobantePago.tipoComprobante"];
            comprobantePago.razonSocial = collection["comprobantePago.razonSocial"];
            comprobantePago.medioPago = collection["comprobantePago.medioPago"];
            comprobantePago.TipoMoneda_ID = int.Parse(collection["comprobantePago.TipoMoneda_ID"]);

            try
            {
                ViewBag.comprobantePago = new ComprobantePagoCN().InsertComprobantePago(comprobantePago);
                // TODO: Add update logic here
                return View();
            }
            catch (Exception e)
            {
                ViewBag.exception = e.Message;
                return View();
            }
        }

        // GET: FacturacionCaja/ComprobantePago/Edit/5
        public ActionResult Edit(int id)
        {
            //return View();
            return RedirectToAction("Index");
        }

        // POST: FacturacionCaja/ComprobantePago/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            return RedirectToAction("Index");

            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: FacturacionCaja/ComprobantePago/Delete/5
        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
            return View();
        }

        // POST: FacturacionCaja/ComprobantePago/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            return RedirectToAction("Index");
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
