﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UPC.TP3.FacturacionCaja.Web.Startup))]
namespace UPC.TP3.FacturacionCaja.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
