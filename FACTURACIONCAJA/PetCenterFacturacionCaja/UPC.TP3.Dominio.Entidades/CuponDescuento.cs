//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.TP3.Dominio.Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class CuponDescuento
    {
        public int CuponDescuento_ID { get; set; }
        public System.DateTime fechaEmision { get; set; }
        public System.DateTime fechaVigencia { get; set; }
        public double porcentajeDescuento { get; set; }
        public string estado { get; set; }
        public System.DateTime fechaUso { get; set; }
        public Nullable<int> ComprobantePago_ID { get; set; }
    
        public virtual ComprobantePago ComprobantePago { get; set; }
    }
}
