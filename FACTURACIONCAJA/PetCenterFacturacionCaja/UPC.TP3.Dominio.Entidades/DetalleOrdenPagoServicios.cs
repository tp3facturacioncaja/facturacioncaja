//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UPC.TP3.Dominio.Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetalleOrdenPagoServicios
    {
        public int Servicios_ID { get; set; }
        public int OrdenPago_ID { get; set; }
        public int cantidad { get; set; }
        public double subTotal { get; set; }
    
        public virtual Servicios Servicios { get; set; }
        public virtual OrdenPago OrdenPago { get; set; }
    }
}
