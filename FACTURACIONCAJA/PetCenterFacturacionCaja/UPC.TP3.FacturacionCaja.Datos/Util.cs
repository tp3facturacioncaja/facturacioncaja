﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace UPC.TP3.FacturacionCaja.Datos
{
    public class Util
    {
        public void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); // Add the original exception as the innerException
            }
        }
    }
}



/* EJEMPLO
 * 
 * public ProtocoloTriaje GetProtocoloTriaje(int tipoPaciente, ICollection<Sintoma> sintomas)
        {
            using (BD_TP2_RICARDOPALMAEntities db = new BD_TP2_RICARDOPALMAEntities())
            {
                List<int> _sintomas = new List<int>();

                foreach (Sintoma _sintoma in sintomas)
                {
                    _sintomas.Add(_sintoma.id);
                }

                ProtocoloTriaje protocoloTriaje = db.ProtocoloTriaje
                                                                    .Where(f => f.tipoPaciente == tipoPaciente)
                                                                    .Where(f => _sintomas.Contains(f.sintoma))
                                                                    .OrderBy(f => f.prioridad)
                                                                    .FirstOrDefault();
                return protocoloTriaje;
            }
        }


    */