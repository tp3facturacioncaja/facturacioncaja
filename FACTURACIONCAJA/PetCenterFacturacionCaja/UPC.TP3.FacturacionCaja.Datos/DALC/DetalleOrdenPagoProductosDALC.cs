﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class DetalleOrdenPagoProductosDALC
    {
        public List<DetalleOrdenPagoProductos> GetDetalleOrdenPagoProductoss()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.DetalleOrdenPagoProductos.ToList();
            }
        }

        public DetalleOrdenPagoProductos GetDetalleOrdenPagoProductosById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.DetalleOrdenPagoProductos.SingleOrDefault(b => b.OrdenPago_ID == id);
            }
        }

        public DetalleOrdenPagoProductos InsertDetalleOrdenPagoProductos(DetalleOrdenPagoProductos DetalleOrdenPagoProductos)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                DetalleOrdenPagoProductos _DetalleOrdenPagoProductos = db.DetalleOrdenPagoProductos.Add(DetalleOrdenPagoProductos);
                new Util().SaveChanges(db);
                return _DetalleOrdenPagoProductos;
            }
        }

        public DetalleOrdenPagoProductos UpdateDetalleOrdenPagoProductos(DetalleOrdenPagoProductos DetalleOrdenPagoProductos)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(DetalleOrdenPagoProductos).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return DetalleOrdenPagoProductos;
            }
        }

    }
}
