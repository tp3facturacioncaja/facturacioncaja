﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class ServiciosDALC
    {
        public List<Servicios> GetServicioss()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.Servicios.ToList();
            }
        }

        public Servicios GetServiciosById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.Servicios.SingleOrDefault(b => b.Servicios_ID == id);
            }
        }

        public Servicios InsertServicios(Servicios Servicios)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                Servicios _Servicios = db.Servicios.Add(Servicios);
                new Util().SaveChanges(db);
                return _Servicios;
            }
        }

        public Servicios UpdateServicios(Servicios Servicios)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(Servicios).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return Servicios;
            }
        }

    }
}
