﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class TipoMonedaDALC
    {
        public List<TipoMoneda> GetTipoMonedas()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.TipoMoneda.ToList();
            }
        }

        public TipoMoneda GetTipoMonedaById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.TipoMoneda.SingleOrDefault(b => b.TipoMoneda_ID == id);
            }
        }

        public TipoMoneda InsertTipoMoneda(TipoMoneda TipoMoneda)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                TipoMoneda _TipoMoneda = db.TipoMoneda.Add(TipoMoneda);
                new Util().SaveChanges(db);
                return _TipoMoneda;
            }
        }

        public TipoMoneda UpdateTipoMoneda(TipoMoneda TipoMoneda)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(TipoMoneda).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return TipoMoneda;
            }
        }

    }
}
