﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class EmpleadoDALC
    {
        public List<Empleado> GetEmpleados()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.Empleado.ToList();
            }
        }

        public Empleado GetEmpleadoById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.Empleado.SingleOrDefault(b => b.Empleado_ID == id);
            }
        }

        public Empleado InsertEmpleado(Empleado Empleado)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                Empleado _Empleado = db.Empleado.Add(Empleado);
                new Util().SaveChanges(db);
                return _Empleado;
            }
        }

        public Empleado UpdateEmpleado(Empleado Empleado)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(Empleado).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return Empleado;
            }
        }

    }
}
