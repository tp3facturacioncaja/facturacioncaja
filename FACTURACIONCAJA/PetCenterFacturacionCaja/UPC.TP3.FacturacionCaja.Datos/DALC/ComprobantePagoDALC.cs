﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class ComprobantePagoDALC
    {
        public List<ComprobantePago> GetComprobantePagos()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.ComprobantePago.ToList();
            }
        }

        public ComprobantePago GetComprobantePagoById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.ComprobantePago.SingleOrDefault(b => b.ComprobantePago_ID == id);
            }
        }

        public ComprobantePago InsertComprobantePago(ComprobantePago ComprobantePago)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                ComprobantePago _ComprobantePago = db.ComprobantePago.Add(ComprobantePago);
                new Util().SaveChanges(db);
                return _ComprobantePago;
            }
        }

        public ComprobantePago UpdateComprobantePago(ComprobantePago ComprobantePago)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(ComprobantePago).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return ComprobantePago;
            }
        }

    }
}
