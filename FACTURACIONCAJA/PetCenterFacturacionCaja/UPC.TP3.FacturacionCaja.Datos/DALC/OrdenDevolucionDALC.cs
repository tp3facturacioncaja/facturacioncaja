﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class OrdenDevolucionDALC
    {
        public List<OrdenDevolucion> GetOrdenDevolucions()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.OrdenDevolucion.ToList();
            }
        }

        public OrdenDevolucion GetOrdenDevolucionById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.OrdenDevolucion.SingleOrDefault(b => b.OrdenDevolucion_ID == id);
            }
        }

        public OrdenDevolucion InsertOrdenDevolucion(OrdenDevolucion OrdenDevolucion)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                OrdenDevolucion _OrdenDevolucion = db.OrdenDevolucion.Add(OrdenDevolucion);
                new Util().SaveChanges(db);
                return _OrdenDevolucion;
            }
        }

        public OrdenDevolucion UpdateOrdenDevolucion(OrdenDevolucion OrdenDevolucion)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(OrdenDevolucion).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return OrdenDevolucion;
            }
        }

    }
}
