﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class CuponDescuentoDALC
    {
        public List<CuponDescuento> GetCuponDescuentos()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.CuponDescuento.ToList();
            }
        }

        public CuponDescuento GetCuponDescuentoById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.CuponDescuento.SingleOrDefault(b => b.CuponDescuento_ID == id);
            }
        }

        public CuponDescuento InsertCuponDescuento(CuponDescuento CuponDescuento)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                CuponDescuento _CuponDescuento = db.CuponDescuento.Add(CuponDescuento);
                new Util().SaveChanges(db);
                return _CuponDescuento;
            }
        }

        public CuponDescuento UpdateCuponDescuento(CuponDescuento CuponDescuento)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(CuponDescuento).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return CuponDescuento;
            }
        }

    }
}
