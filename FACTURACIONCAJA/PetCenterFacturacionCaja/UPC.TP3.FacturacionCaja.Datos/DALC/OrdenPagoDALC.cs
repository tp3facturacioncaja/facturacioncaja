﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class OrdenPagoDALC
    {
        public List<OrdenPago> GetOrdenPagos()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.OrdenPago.ToList();
            }
        }

        public OrdenPago GetOrdenPagoById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.OrdenPago.SingleOrDefault(b => b.OrdenPago_ID == id);
            }
        }

        public OrdenPago InsertOrdenPago(OrdenPago OrdenPago)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                OrdenPago _OrdenPago = db.OrdenPago.Add(OrdenPago);
                new Util().SaveChanges(db);
                return _OrdenPago;
            }
        }

        public OrdenPago UpdateOrdenPago(OrdenPago OrdenPago)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(OrdenPago).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return OrdenPago;
            }
        }

    }
}
