﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class DetalleOrdenPagoServiciosDALC
    {
        public List<DetalleOrdenPagoServicios> GetDetalleOrdenPagoServicioss()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.DetalleOrdenPagoServicios.ToList();
            }
        }

        public DetalleOrdenPagoServicios GetDetalleOrdenPagoServiciosById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.DetalleOrdenPagoServicios.SingleOrDefault(b => b.OrdenPago_ID == id);
            }
        }

        public DetalleOrdenPagoServicios InsertDetalleOrdenPagoServicios(DetalleOrdenPagoServicios DetalleOrdenPagoServicios)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                DetalleOrdenPagoServicios _DetalleOrdenPagoServicios = db.DetalleOrdenPagoServicios.Add(DetalleOrdenPagoServicios);
                new Util().SaveChanges(db);
                return _DetalleOrdenPagoServicios;
            }
        }

        public DetalleOrdenPagoServicios UpdateDetalleOrdenPagoServicios(DetalleOrdenPagoServicios DetalleOrdenPagoServicios)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(DetalleOrdenPagoServicios).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return DetalleOrdenPagoServicios;
            }
        }

    }
}
