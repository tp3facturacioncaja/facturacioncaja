﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class ProductosDALC
    {
        public List<Productos> GetProductoss()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.Productos.ToList();
            }
        }

        public Productos GetProductosById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.Productos.SingleOrDefault(b => b.Productos_ID == id);
            }
        }

        public Productos InsertProductos(Productos Productos)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                Productos _Productos = db.Productos.Add(Productos);
                new Util().SaveChanges(db);
                return _Productos;
            }
        }

        public Productos UpdateProductos(Productos Productos)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(Productos).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return Productos;
            }
        }

    }
}
