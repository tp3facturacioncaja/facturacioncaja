﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;

namespace UPC.TP3.FacturacionCaja.Datos.DALC
{
    public class TurnoCajaDALC
    {
        public List<TurnoCaja> GetTurnoCajas()
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.TurnoCaja.ToList();
            }
        }

        public TurnoCaja GetTurnoCajaById(int id)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                return db.TurnoCaja.SingleOrDefault(b => b.TurnoCaja_ID == id);
            }
        }

        public TurnoCaja InsertTurnoCaja(TurnoCaja TurnoCaja)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                TurnoCaja _TurnoCaja = db.TurnoCaja.Add(TurnoCaja);
                new Util().SaveChanges(db);
                return _TurnoCaja;
            }
        }

        public TurnoCaja UpdateTurnoCaja(TurnoCaja TurnoCaja)
        {
            using (PetCenterDbEntities db = new PetCenterDbEntities())
            {
                db.Entry(TurnoCaja).State = System.Data.Entity.EntityState.Modified;
                new Util().SaveChanges(db);
                return TurnoCaja;
            }
        }

    }
}
