﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class ProductosCN
    {
        public List<Productos> GetProductoss()
        {
            ProductosDALC obj = new ProductosDALC();
            return obj.GetProductoss();
        }

        public Productos GetProductosById(int id)
        {
            ProductosDALC obj = new ProductosDALC();
            return obj.GetProductosById(id);
        }

        public Productos InsertProductos(Productos Productos)
        {
            ProductosDALC obj = new ProductosDALC();
            return obj.InsertProductos(Productos);
        }
        public Productos UpdateProductos(Productos Productos)
        {
            ProductosDALC obj = new ProductosDALC();
            return obj.UpdateProductos(Productos);
        }
    }
}
