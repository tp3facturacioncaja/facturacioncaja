﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class EmpleadoCN
    {
        public List<Empleado> GetEmpleados()
        {
            EmpleadoDALC obj = new EmpleadoDALC();
            return obj.GetEmpleados();
        }

        public Empleado GetEmpleadoById(int id)
        {
            EmpleadoDALC obj = new EmpleadoDALC();
            return obj.GetEmpleadoById(id);
        }

        public Empleado InsertEmpleado(Empleado Empleado)
        {
            EmpleadoDALC obj = new EmpleadoDALC();
            return obj.InsertEmpleado(Empleado);
        }
        public Empleado UpdateEmpleado(Empleado Empleado)
        {
            EmpleadoDALC obj = new EmpleadoDALC();
            return obj.UpdateEmpleado(Empleado);
        }
    }
}
