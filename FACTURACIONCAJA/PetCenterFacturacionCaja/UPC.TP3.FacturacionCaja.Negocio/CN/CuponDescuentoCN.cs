﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class CuponDescuentoCN
    {
        public List<CuponDescuento> GetCuponDescuentos()
        {
            CuponDescuentoDALC obj = new CuponDescuentoDALC();
            return obj.GetCuponDescuentos();
        }

        public CuponDescuento GetCuponDescuentoById(int id)
        {
            CuponDescuentoDALC obj = new CuponDescuentoDALC();
            return obj.GetCuponDescuentoById(id);
        }

        public CuponDescuento InsertCuponDescuento(CuponDescuento CuponDescuento)
        {
            CuponDescuentoDALC obj = new CuponDescuentoDALC();
            return obj.InsertCuponDescuento(CuponDescuento);
        }
        public CuponDescuento UpdateCuponDescuento(CuponDescuento CuponDescuento)
        {
            CuponDescuentoDALC obj = new CuponDescuentoDALC();
            return obj.UpdateCuponDescuento(CuponDescuento);
        }
    }
}
