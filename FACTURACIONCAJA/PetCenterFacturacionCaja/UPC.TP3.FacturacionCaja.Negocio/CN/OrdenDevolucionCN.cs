﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class OrdenDevolucionCN
    {
        public List<OrdenDevolucion> GetOrdenDevolucions()
        {
            OrdenDevolucionDALC obj = new OrdenDevolucionDALC();
            return obj.GetOrdenDevolucions();
        }

        public OrdenDevolucion GetOrdenDevolucionById(int id)
        {
            OrdenDevolucionDALC obj = new OrdenDevolucionDALC();
            return obj.GetOrdenDevolucionById(id);
        }

        public OrdenDevolucion InsertOrdenDevolucion(OrdenDevolucion OrdenDevolucion)
        {
            OrdenDevolucionDALC obj = new OrdenDevolucionDALC();
            return obj.InsertOrdenDevolucion(OrdenDevolucion);
        }
        public OrdenDevolucion UpdateOrdenDevolucion(OrdenDevolucion OrdenDevolucion)
        {
            OrdenDevolucionDALC obj = new OrdenDevolucionDALC();
            return obj.UpdateOrdenDevolucion(OrdenDevolucion);
        }
    }
}
