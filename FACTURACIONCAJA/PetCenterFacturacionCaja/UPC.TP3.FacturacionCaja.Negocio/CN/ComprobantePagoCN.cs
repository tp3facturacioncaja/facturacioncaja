﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class ComprobantePagoCN
    {
        public List<ComprobantePago> GetComprobantePagos()
        {
            ComprobantePagoDALC obj = new ComprobantePagoDALC();
            return obj.GetComprobantePagos();
        }

        public ComprobantePago GetComprobantePagoById(int id)
        {
            ComprobantePagoDALC obj = new ComprobantePagoDALC();
            return obj.GetComprobantePagoById(id);
        }

        public ComprobantePago InsertComprobantePago(ComprobantePago comprobantePago)
        {
            comprobantePago.fecha = DateTime.Now;
            comprobantePago.hora = DateTime.Now;
            comprobantePago.estado = "Emitido";
            comprobantePago.tipoCambio = Double.Parse("");
            comprobantePago.montoComprobante = Double.Parse("");
            comprobantePago.nroDocumento = "";
            comprobantePago.igv = Double.Parse("");
            comprobantePago.vuelto = Double.Parse("");
            comprobantePago.Empleado_ID = 11;
            comprobantePago.TurnoCaja_ID = 3;
            comprobantePago.montoDescuento = Double.Parse("");


            ComprobantePagoDALC obj = new ComprobantePagoDALC();
            return obj.InsertComprobantePago(comprobantePago);
        }
        public ComprobantePago UpdateComprobantePago(ComprobantePago comprobantePago)
        {
            ComprobantePagoDALC obj = new ComprobantePagoDALC();
            return obj.UpdateComprobantePago(comprobantePago);
        }
    }
}
