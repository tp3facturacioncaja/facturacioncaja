﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class OrdenPagoCN
    {
        public List<OrdenPago> GetOrdenPagos()
        {
            OrdenPagoDALC obj = new OrdenPagoDALC();
            return obj.GetOrdenPagos();
        }

        public OrdenPago GetOrdenPagoById(int id)
        {
            OrdenPagoDALC obj = new OrdenPagoDALC();
            return obj.GetOrdenPagoById(id);
        }

        public OrdenPago InsertOrdenPago(OrdenPago OrdenPago)
        {
            OrdenPagoDALC obj = new OrdenPagoDALC();
            return obj.InsertOrdenPago(OrdenPago);
        }
        public OrdenPago UpdateOrdenPago(OrdenPago OrdenPago)
        {
            OrdenPagoDALC obj = new OrdenPagoDALC();
            return obj.UpdateOrdenPago(OrdenPago);
        }
    }
}
