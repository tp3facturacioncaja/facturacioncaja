﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class TurnoCajaCN
    {
        public List<TurnoCaja> GetTurnoCajas()
        {
            TurnoCajaDALC obj = new TurnoCajaDALC();
            return obj.GetTurnoCajas();
        }

        public TurnoCaja GetTurnoCajaById(int id)
        {
            TurnoCajaDALC obj = new TurnoCajaDALC();
            return obj.GetTurnoCajaById(id);
        }

        public TurnoCaja InsertTurnoCaja(TurnoCaja TurnoCaja)
        {
            TurnoCajaDALC obj = new TurnoCajaDALC();
            return obj.InsertTurnoCaja(TurnoCaja);
        }
        public TurnoCaja UpdateTurnoCaja(TurnoCaja TurnoCaja)
        {
            TurnoCajaDALC obj = new TurnoCajaDALC();
            return obj.UpdateTurnoCaja(TurnoCaja);
        }
    }
}
