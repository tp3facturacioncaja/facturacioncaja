﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class ServiciosCN
    {
        public List<Servicios> GetServicioss()
        {
            ServiciosDALC obj = new ServiciosDALC();
            return obj.GetServicioss();
        }

        public Servicios GetServiciosById(int id)
        {
            ServiciosDALC obj = new ServiciosDALC();
            return obj.GetServiciosById(id);
        }

        public Servicios InsertServicios(Servicios Servicios)
        {
            ServiciosDALC obj = new ServiciosDALC();
            return obj.InsertServicios(Servicios);
        }
        public Servicios UpdateServicios(Servicios Servicios)
        {
            ServiciosDALC obj = new ServiciosDALC();
            return obj.UpdateServicios(Servicios);
        }
    }
}
