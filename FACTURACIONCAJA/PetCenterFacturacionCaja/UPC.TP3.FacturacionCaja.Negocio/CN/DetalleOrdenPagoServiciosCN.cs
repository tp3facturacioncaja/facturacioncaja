﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class DetalleOrdenPagoServiciosCN
    {
        public List<DetalleOrdenPagoServicios> GetDetalleOrdenPagoServicioss()
        {
            DetalleOrdenPagoServiciosDALC obj = new DetalleOrdenPagoServiciosDALC();
            return obj.GetDetalleOrdenPagoServicioss();
        }

        public DetalleOrdenPagoServicios GetDetalleOrdenPagoServiciosById(int id)
        {
            DetalleOrdenPagoServiciosDALC obj = new DetalleOrdenPagoServiciosDALC();
            return obj.GetDetalleOrdenPagoServiciosById(id);
        }

        public DetalleOrdenPagoServicios InsertDetalleOrdenPagoServicios(DetalleOrdenPagoServicios DetalleOrdenPagoServicios)
        {
            DetalleOrdenPagoServiciosDALC obj = new DetalleOrdenPagoServiciosDALC();
            return obj.InsertDetalleOrdenPagoServicios(DetalleOrdenPagoServicios);
        }
        public DetalleOrdenPagoServicios UpdateDetalleOrdenPagoServicios(DetalleOrdenPagoServicios DetalleOrdenPagoServicios)
        {
            DetalleOrdenPagoServiciosDALC obj = new DetalleOrdenPagoServiciosDALC();
            return obj.UpdateDetalleOrdenPagoServicios(DetalleOrdenPagoServicios);
        }
    }
}
