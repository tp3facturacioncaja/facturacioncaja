﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class DetalleOrdenPagoProductosCN
    {
        public List<DetalleOrdenPagoProductos> GetDetalleOrdenPagoProductoss()
        {
            DetalleOrdenPagoProductosDALC obj = new DetalleOrdenPagoProductosDALC();
            return obj.GetDetalleOrdenPagoProductoss();
        }

        public DetalleOrdenPagoProductos GetDetalleOrdenPagoProductosById(int id)
        {
            DetalleOrdenPagoProductosDALC obj = new DetalleOrdenPagoProductosDALC();
            return obj.GetDetalleOrdenPagoProductosById(id);
        }

        public DetalleOrdenPagoProductos InsertDetalleOrdenPagoProductos(DetalleOrdenPagoProductos DetalleOrdenPagoProductos)
        {
            DetalleOrdenPagoProductosDALC obj = new DetalleOrdenPagoProductosDALC();
            return obj.InsertDetalleOrdenPagoProductos(DetalleOrdenPagoProductos);
        }
        public DetalleOrdenPagoProductos UpdateDetalleOrdenPagoProductos(DetalleOrdenPagoProductos DetalleOrdenPagoProductos)
        {
            DetalleOrdenPagoProductosDALC obj = new DetalleOrdenPagoProductosDALC();
            return obj.UpdateDetalleOrdenPagoProductos(DetalleOrdenPagoProductos);
        }
    }
}
