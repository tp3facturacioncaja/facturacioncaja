﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPC.TP3.Dominio.Entidades;
using UPC.TP3.FacturacionCaja.Datos.DALC;

namespace UPC.TP3.FacturacionCaja.Negocio.CN
{
    public class TipoMonedaCN
    {
        public List<TipoMoneda> GetTipoMonedas()
        {
            TipoMonedaDALC obj = new TipoMonedaDALC();
            return obj.GetTipoMonedas();
        }

        public TipoMoneda GetTipoMonedaById(int id)
        {
            TipoMonedaDALC obj = new TipoMonedaDALC();
            return obj.GetTipoMonedaById(id);
        }

        public TipoMoneda InsertTipoMoneda(TipoMoneda TipoMoneda)
        {
            TipoMonedaDALC obj = new TipoMonedaDALC();
            return obj.InsertTipoMoneda(TipoMoneda);
        }
        public TipoMoneda UpdateTipoMoneda(TipoMoneda TipoMoneda)
        {
            TipoMonedaDALC obj = new TipoMonedaDALC();
            return obj.UpdateTipoMoneda(TipoMoneda);
        }
    }
}
