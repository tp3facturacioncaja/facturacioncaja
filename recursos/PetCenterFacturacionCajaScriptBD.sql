USE [PetCenterDb]
GO
/****** Object:  Table [dbo].[TipoMoneda]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoMoneda](
	[TipoMoneda_ID] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](255) NOT NULL,
	[abreviatura] [varchar](255) NOT NULL,
	[simbologia] [varchar](255) NOT NULL,
 CONSTRAINT [PK_TipoMoneda27] PRIMARY KEY NONCLUSTERED 
(
	[TipoMoneda_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aplicacion]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aplicacion](
	[AplicacionId] [bigint] IDENTITY(1,1) NOT NULL,
	[AplicacionIdPadre] [bigint] NULL,
	[Nombre] [varchar](70) NULL,
	[Descripcion] [varchar](70) NULL,
	[Controlador] [varchar](70) NULL,
	[Accion] [varchar](70) NULL,
	[EsMenu] [int] NULL,
	[Orden] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AplicacionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Caja]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Caja](
	[Caja_ID] [int] IDENTITY(1,1) NOT NULL,
	[nombreCaja] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Caja_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado](
	[Empleado_ID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NOT NULL,
	[cargo] [varchar](255) NOT NULL,
	[usuario] [varchar](255) NOT NULL,
	[password] [varchar](255) NOT NULL,
	[tipoDocumento] [varchar](255) NOT NULL,
	[nroDocumento] [varchar](255) NOT NULL,
	[sede] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Empleado20] PRIMARY KEY NONCLUSTERED 
(
	[Empleado_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Servicios]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Servicios](
	[Servicios_ID] [int] IDENTITY(1,1) NOT NULL,
	[nombreServicio] [varchar](255) NOT NULL,
	[precioUnitario] [float] NOT NULL,
 CONSTRAINT [PK_Servicios23] PRIMARY KEY NONCLUSTERED 
(
	[Servicios_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[Productos_ID] [int] IDENTITY(1,1) NOT NULL,
	[nombreProducto] [varchar](255) NOT NULL,
	[stock] [int] NOT NULL,
	[precioUnitario] [float] NOT NULL,
 CONSTRAINT [PK_Productos24] PRIMARY KEY NONCLUSTERED 
(
	[Productos_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TurnoCaja]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TurnoCaja](
	[TurnoCaja_ID] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[apertura] [float] NOT NULL,
	[horaApertura] [datetime] NULL,
	[cierre] [float] NOT NULL,
	[horaCierre] [datetime] NULL,
	[estado] [varchar](255) NOT NULL,
	[aprobacionApertura] [varchar](255) NOT NULL,
	[aprobacionCierre] [varchar](255) NOT NULL,
	[cierreApertura] [datetime] NULL,
	[flagArqueo] [int] NOT NULL,
	[cajero] [varchar](255) NOT NULL,
	[Caja_ID] [int] NULL,
 CONSTRAINT [PK_TurnoCaja26] PRIMARY KEY NONCLUSTERED 
(
	[TurnoCaja_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Devolucion]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Devolucion](
	[Devolucion_ID] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[monto] [float] NOT NULL,
	[concepto] [varchar](255) NOT NULL,
	[Empleado_ID] [int] NOT NULL,
 CONSTRAINT [PK_Devolucion21] PRIMARY KEY NONCLUSTERED 
(
	[Devolucion_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Arqueo]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Arqueo](
	[Arqueo_ID] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NULL,
	[turnoCaja] [int] NOT NULL,
	[montoInicial] [float] NOT NULL,
	[totalSistemaSoles] [float] NOT NULL,
	[totalSistemaDolares] [float] NOT NULL,
	[totalDepositos] [float] NOT NULL,
	[totalCajaSoles] [float] NOT NULL,
	[totalCajaDolares] [float] NOT NULL,
	[observaciones] [varchar](255) NOT NULL,
	[arqueoSoles] [float] NOT NULL,
	[arqueoDolares] [float] NOT NULL,
	[supervisor] [varchar](255) NOT NULL,
	[empleado] [varchar](255) NOT NULL,
	[Empleado_ID] [int] NOT NULL,
	[Empleado_Empleado_ID] [int] NULL,
	[COL_1] [int] NULL,
 CONSTRAINT [PK_Arqueo19] PRIMARY KEY NONCLUSTERED 
(
	[Arqueo_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ComprobantePago]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ComprobantePago](
	[ComprobantePago_ID] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[hora] [datetime] NOT NULL,
	[montoRecibido] [float] NOT NULL,
	[estado] [varchar](255) NOT NULL,
	[tipoCambio] [float] NOT NULL,
	[montoComprobante] [float] NOT NULL,
	[tipoComprobante] [varchar](255) NOT NULL,
	[nroDocumento] [varchar](255) NOT NULL,
	[razonSocial] [varchar](255) NOT NULL,
	[igv] [float] NOT NULL,
	[vuelto] [float] NOT NULL,
	[medioPago] [varchar](255) NOT NULL,
	[montoDescuento] [float] NOT NULL,
	[Empleado_ID] [int] NOT NULL,
	[TurnoCaja_ID] [int] NOT NULL,
	[TipoMoneda_ID] [int] NOT NULL,
	[nrodocumentoComprobante] [varchar](15) NULL,
 CONSTRAINT [PK_ComprobantePago18] PRIMARY KEY NONCLUSTERED 
(
	[ComprobantePago_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[prc_sel_apertura_caja]    Script Date: 03/26/2017 19:51:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[prc_sel_apertura_caja]
as
SELECT 
 TC.TurnoCaja_ID
,C.nombreCaja
,TC.cajero
,CONVERT(VARCHAR(10),TC.horaApertura,103) fechaApertura
,CONVERT(VARCHAR(8),TC.horaApertura,108) horaApertura
,0 cantidadArqueo
,DATEDIFF(HOUR,tc.horaApertura,GETDATE()) intervalo
FROM TurnoCaja TC 
INNER JOIN Caja C
ON TC.Caja_ID = C.Caja_ID
GO
/****** Object:  Table [dbo].[TurnoCajaxMoneda]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TurnoCajaxMoneda](
	[TipoMoneda_ID] [int] NOT NULL,
	[TurnoCaja_ID] [int] NOT NULL,
	[montoInicial] [float] NOT NULL,
	[montoFinal] [float] NOT NULL,
 CONSTRAINT [PK_TurnoCajaxMoneda28] PRIMARY KEY NONCLUSTERED 
(
	[TipoMoneda_ID] ASC,
	[TurnoCaja_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TurnoCajaEmpleado]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TurnoCajaEmpleado](
	[Empleado_ID] [int] NOT NULL,
	[TurnoCaja_ID] [int] NOT NULL,
 CONSTRAINT [PK_232] PRIMARY KEY NONCLUSTERED 
(
	[Empleado_ID] ASC,
	[TurnoCaja_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TurnoCajaArqueo]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TurnoCajaArqueo](
	[TurnoCaja_ID] [int] NOT NULL,
	[Arqueo_ID] [int] NOT NULL,
 CONSTRAINT [PK_333] PRIMARY KEY NONCLUSTERED 
(
	[TurnoCaja_ID] ASC,
	[Arqueo_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Deposito]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Deposito](
	[Deposito_ID] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [smallint] NOT NULL,
	[monto] [float] NOT NULL,
	[descripcion] [varchar](255) NOT NULL,
	[TurnoCaja_ID] [int] NOT NULL,
	[TipoMoneda_ID] [int] NULL,
 CONSTRAINT [PK_Deposito17] PRIMARY KEY NONCLUSTERED 
(
	[Deposito_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[sp_consulta_comprobante]    Script Date: 03/26/2017 19:51:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_consulta_comprobante]
as
SELECT *
  FROM [PetCenterDb].[dbo].ComprobantePago
GO
/****** Object:  Table [dbo].[CuponDescuento]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CuponDescuento](
	[CuponDescuento_ID] [int] IDENTITY(1,1) NOT NULL,
	[fechaEmision] [datetime] NOT NULL,
	[fechaVigencia] [datetime] NOT NULL,
	[porcentajeDescuento] [float] NOT NULL,
	[estado] [varchar](255) NOT NULL,
	[fechaUso] [datetime] NOT NULL,
	[ComprobantePago_ID] [int] NULL,
 CONSTRAINT [PK_CuponDescuento29] PRIMARY KEY NONCLUSTERED 
(
	[CuponDescuento_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [TC_CuponDescuento57] UNIQUE NONCLUSTERED 
(
	[ComprobantePago_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdenPago]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrdenPago](
	[OrdenPago_ID] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[hora] [datetime] NOT NULL,
	[estado] [varchar](255) NOT NULL,
	[nombreCliente] [varchar](255) NOT NULL,
	[nombreMascota] [varchar](255) NOT NULL,
	[direccionCliente] [varchar](255) NOT NULL,
	[tipoMascota] [varchar](255) NOT NULL,
	[importeTotal] [float] NOT NULL,
	[ComprobantePago_ID] [int] NULL,
 CONSTRAINT [PK_OrdenPago25] PRIMARY KEY NONCLUSTERED 
(
	[OrdenPago_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdenDevolucion]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrdenDevolucion](
	[OrdenDevolucion_ID] [int] IDENTITY(1,1) NOT NULL,
	[fechaEmision] [datetime] NOT NULL,
	[importe] [float] NOT NULL,
	[ComprobantePago_ID] [int] NOT NULL,
	[Empleado_ID] [int] NOT NULL,
	[TurnoCaja_ID] [int] NOT NULL,
 CONSTRAINT [PK_OrdenDevolucion22] PRIMARY KEY NONCLUSTERED 
(
	[OrdenDevolucion_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleOrdenPagoServicios]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleOrdenPagoServicios](
	[Servicios_ID] [int] NOT NULL,
	[OrdenPago_ID] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[subTotal] [float] NOT NULL,
 CONSTRAINT [PK_DetalleOrdenPagoServicios30] PRIMARY KEY NONCLUSTERED 
(
	[Servicios_ID] ASC,
	[OrdenPago_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleOrdenPagoProductos]    Script Date: 03/26/2017 19:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleOrdenPagoProductos](
	[OrdenPago_ID] [int] NOT NULL,
	[Productos_ID] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[subTotal] [float] NOT NULL,
 CONSTRAINT [PK_DetalleOrdenPagoProductos31] PRIMARY KEY NONCLUSTERED 
(
	[OrdenPago_ID] ASC,
	[Productos_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Arqueo29]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[Arqueo]  WITH CHECK ADD  CONSTRAINT [FK_Arqueo29] FOREIGN KEY([Empleado_ID])
REFERENCES [dbo].[Empleado] ([Empleado_ID])
GO
ALTER TABLE [dbo].[Arqueo] CHECK CONSTRAINT [FK_Arqueo29]
GO
/****** Object:  ForeignKey [FK_Arqueo35]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[Arqueo]  WITH CHECK ADD  CONSTRAINT [FK_Arqueo35] FOREIGN KEY([Empleado_Empleado_ID])
REFERENCES [dbo].[Empleado] ([Empleado_ID])
GO
ALTER TABLE [dbo].[Arqueo] CHECK CONSTRAINT [FK_Arqueo35]
GO
/****** Object:  ForeignKey [FK_Arqueo36]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[Arqueo]  WITH CHECK ADD  CONSTRAINT [FK_Arqueo36] FOREIGN KEY([COL_1])
REFERENCES [dbo].[Empleado] ([Empleado_ID])
GO
ALTER TABLE [dbo].[Arqueo] CHECK CONSTRAINT [FK_Arqueo36]
GO
/****** Object:  ForeignKey [FK__Comproban__TipoM__29572725]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[ComprobantePago]  WITH CHECK ADD FOREIGN KEY([TipoMoneda_ID])
REFERENCES [dbo].[TipoMoneda] ([TipoMoneda_ID])
GO
/****** Object:  ForeignKey [FK__Comproban__TipoM__2A4B4B5E]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[ComprobantePago]  WITH CHECK ADD FOREIGN KEY([TipoMoneda_ID])
REFERENCES [dbo].[TipoMoneda] ([TipoMoneda_ID])
GO
/****** Object:  ForeignKey [FK_ComprobantePago31]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[ComprobantePago]  WITH CHECK ADD  CONSTRAINT [FK_ComprobantePago31] FOREIGN KEY([Empleado_ID])
REFERENCES [dbo].[Empleado] ([Empleado_ID])
GO
ALTER TABLE [dbo].[ComprobantePago] CHECK CONSTRAINT [FK_ComprobantePago31]
GO
/****** Object:  ForeignKey [FK_ComprobantePago42]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[ComprobantePago]  WITH CHECK ADD  CONSTRAINT [FK_ComprobantePago42] FOREIGN KEY([TurnoCaja_ID])
REFERENCES [dbo].[TurnoCaja] ([TurnoCaja_ID])
GO
ALTER TABLE [dbo].[ComprobantePago] CHECK CONSTRAINT [FK_ComprobantePago42]
GO
/****** Object:  ForeignKey [FK_CuponDescuento28]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[CuponDescuento]  WITH CHECK ADD  CONSTRAINT [FK_CuponDescuento28] FOREIGN KEY([ComprobantePago_ID])
REFERENCES [dbo].[ComprobantePago] ([ComprobantePago_ID])
GO
ALTER TABLE [dbo].[CuponDescuento] CHECK CONSTRAINT [FK_CuponDescuento28]
GO
/****** Object:  ForeignKey [FK_Deposito41]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[Deposito]  WITH CHECK ADD  CONSTRAINT [FK_Deposito41] FOREIGN KEY([TurnoCaja_ID])
REFERENCES [dbo].[TurnoCaja] ([TurnoCaja_ID])
GO
ALTER TABLE [dbo].[Deposito] CHECK CONSTRAINT [FK_Deposito41]
GO
/****** Object:  ForeignKey [FK_DetalleOrdenPagoProductos39]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[DetalleOrdenPagoProductos]  WITH CHECK ADD  CONSTRAINT [FK_DetalleOrdenPagoProductos39] FOREIGN KEY([OrdenPago_ID])
REFERENCES [dbo].[OrdenPago] ([OrdenPago_ID])
GO
ALTER TABLE [dbo].[DetalleOrdenPagoProductos] CHECK CONSTRAINT [FK_DetalleOrdenPagoProductos39]
GO
/****** Object:  ForeignKey [FK_DetalleOrdenPagoProductos40]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[DetalleOrdenPagoProductos]  WITH CHECK ADD  CONSTRAINT [FK_DetalleOrdenPagoProductos40] FOREIGN KEY([Productos_ID])
REFERENCES [dbo].[Productos] ([Productos_ID])
GO
ALTER TABLE [dbo].[DetalleOrdenPagoProductos] CHECK CONSTRAINT [FK_DetalleOrdenPagoProductos40]
GO
/****** Object:  ForeignKey [FK_DetalleOrdenPagoServicios37]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[DetalleOrdenPagoServicios]  WITH CHECK ADD  CONSTRAINT [FK_DetalleOrdenPagoServicios37] FOREIGN KEY([Servicios_ID])
REFERENCES [dbo].[Servicios] ([Servicios_ID])
GO
ALTER TABLE [dbo].[DetalleOrdenPagoServicios] CHECK CONSTRAINT [FK_DetalleOrdenPagoServicios37]
GO
/****** Object:  ForeignKey [FK_DetalleOrdenPagoServicios38]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[DetalleOrdenPagoServicios]  WITH CHECK ADD  CONSTRAINT [FK_DetalleOrdenPagoServicios38] FOREIGN KEY([OrdenPago_ID])
REFERENCES [dbo].[OrdenPago] ([OrdenPago_ID])
GO
ALTER TABLE [dbo].[DetalleOrdenPagoServicios] CHECK CONSTRAINT [FK_DetalleOrdenPagoServicios38]
GO
/****** Object:  ForeignKey [FK_Devolucion30]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[Devolucion]  WITH CHECK ADD  CONSTRAINT [FK_Devolucion30] FOREIGN KEY([Empleado_ID])
REFERENCES [dbo].[Empleado] ([Empleado_ID])
GO
ALTER TABLE [dbo].[Devolucion] CHECK CONSTRAINT [FK_Devolucion30]
GO
/****** Object:  ForeignKey [FK_OrdenDevolucion27]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[OrdenDevolucion]  WITH CHECK ADD  CONSTRAINT [FK_OrdenDevolucion27] FOREIGN KEY([ComprobantePago_ID])
REFERENCES [dbo].[ComprobantePago] ([ComprobantePago_ID])
GO
ALTER TABLE [dbo].[OrdenDevolucion] CHECK CONSTRAINT [FK_OrdenDevolucion27]
GO
/****** Object:  ForeignKey [FK_OrdenDevolucion34]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[OrdenDevolucion]  WITH CHECK ADD  CONSTRAINT [FK_OrdenDevolucion34] FOREIGN KEY([Empleado_ID])
REFERENCES [dbo].[Empleado] ([Empleado_ID])
GO
ALTER TABLE [dbo].[OrdenDevolucion] CHECK CONSTRAINT [FK_OrdenDevolucion34]
GO
/****** Object:  ForeignKey [FK_OrdenDevolucion45]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[OrdenDevolucion]  WITH CHECK ADD  CONSTRAINT [FK_OrdenDevolucion45] FOREIGN KEY([TurnoCaja_ID])
REFERENCES [dbo].[TurnoCaja] ([TurnoCaja_ID])
GO
ALTER TABLE [dbo].[OrdenDevolucion] CHECK CONSTRAINT [FK_OrdenDevolucion45]
GO
/****** Object:  ForeignKey [FK_OrdenPago26]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[OrdenPago]  WITH CHECK ADD  CONSTRAINT [FK_OrdenPago26] FOREIGN KEY([ComprobantePago_ID])
REFERENCES [dbo].[ComprobantePago] ([ComprobantePago_ID])
GO
ALTER TABLE [dbo].[OrdenPago] CHECK CONSTRAINT [FK_OrdenPago26]
GO
/****** Object:  ForeignKey [FK__TurnoCaja__Caja___37A5467C]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[TurnoCaja]  WITH CHECK ADD FOREIGN KEY([Caja_ID])
REFERENCES [dbo].[Caja] ([Caja_ID])
GO
/****** Object:  ForeignKey [FK_343]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[TurnoCajaArqueo]  WITH CHECK ADD  CONSTRAINT [FK_343] FOREIGN KEY([TurnoCaja_ID])
REFERENCES [dbo].[TurnoCaja] ([TurnoCaja_ID])
GO
ALTER TABLE [dbo].[TurnoCajaArqueo] CHECK CONSTRAINT [FK_343]
GO
/****** Object:  ForeignKey [FK_344]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[TurnoCajaArqueo]  WITH CHECK ADD  CONSTRAINT [FK_344] FOREIGN KEY([Arqueo_ID])
REFERENCES [dbo].[Arqueo] ([Arqueo_ID])
GO
ALTER TABLE [dbo].[TurnoCajaArqueo] CHECK CONSTRAINT [FK_344]
GO
/****** Object:  ForeignKey [FK_232]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[TurnoCajaEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_232] FOREIGN KEY([Empleado_ID])
REFERENCES [dbo].[Empleado] ([Empleado_ID])
GO
ALTER TABLE [dbo].[TurnoCajaEmpleado] CHECK CONSTRAINT [FK_232]
GO
/****** Object:  ForeignKey [FK_233]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[TurnoCajaEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_233] FOREIGN KEY([TurnoCaja_ID])
REFERENCES [dbo].[TurnoCaja] ([TurnoCaja_ID])
GO
ALTER TABLE [dbo].[TurnoCajaEmpleado] CHECK CONSTRAINT [FK_233]
GO
/****** Object:  ForeignKey [FK_TurnoCajaxMoneda49]    Script Date: 03/26/2017 19:51:30 ******/
ALTER TABLE [dbo].[TurnoCajaxMoneda]  WITH CHECK ADD  CONSTRAINT [FK_TurnoCajaxMoneda49] FOREIGN KEY([TurnoCaja_ID])
REFERENCES [dbo].[TurnoCaja] ([TurnoCaja_ID])
GO
ALTER TABLE [dbo].[TurnoCajaxMoneda] CHECK CONSTRAINT [FK_TurnoCajaxMoneda49]
GO
